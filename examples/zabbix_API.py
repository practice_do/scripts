import requests
from zbx_creds import *

headers = {"Content-Type": "application/json-rpc"}
url = ""  # zabbix_url

login_json = {
    "jsonrpc": "2.0",
    "method": "user.login",
    "params": {
        "user": username,
        "password": pw
    },
    "id": 1
}


def login_in():
    login = requests.post(url=url, headers=headers, json=login_json)
    return login.json()['result']


def logout(token):
    logout_json = {
        "jsonrpc": "2.0",
        "method": "user.logout",
        "params": [],
        "id": 1,
        "auth": token
    }
    return requests.post(url=url, headers=headers, json=logout_json)


def get_group_id(group_name, token):
    group_id_json = {
        "jsonrpc": "2.0",
        "method": "hostgroup.get",
        "params": {
            "output": "extend",
            "filter": {
                "name": [
                    group_name,
                ]
            }
        },
        "auth": token,
        "id": 1
    }
    get_group_id = requests.post(url=url, headers=headers, json=group_id_json)
    return get_group_id.json()['result'][0]['groupid']


def get_template_id(token):
    template_id_json = {
        "jsonrpc": "2.0",
        "method": "template.get",
        "params": {
            "output": "extend",
            "filter": {
                "host": [
                    ""  # template_name
                ]
            }
        },
        "auth": token,
        "id": 1
    }
    get_teplate_id = requests.post(url=url, headers=headers, json=template_id_json)
    return get_teplate_id.json()['result'][0]['templateid']


def get_interface_id(host_id, token):
    get_interface_id_json = {
        "jsonrpc": "2.0",
        "method": "hostinterface.get",
        "params": {
            "output": "extend",
            "hostids": host_id
        },
        "auth": token,
        "id": 1
    }
    get_interface_id = requests.post(url=url, headers=headers, json=get_interface_id_json)
    return get_interface_id.json()['result'][0]['interfaceid']


def get_item_id(host_id, interface_id, token):
    add_cpu_load_json = {
        "jsonrpc": "2.0",
        "method": "item.create",
        "params": {
            "name": "CPU load",
            "key_": "cpu.load",
            "hostid": host_id,
            "type": 20,
            "value_type": 0,
            "interfaceid": interface_id,
            "delay": "5s",
            "snmp_oid": "",  # snmp_id
            "description": ""  # descr
        },
        "auth": token,
        "id": 1
    }
    add_cpu_load_item = requests.post(url=url, headers=headers, json=add_cpu_load_json)
    return add_cpu_load_item.json()['result']['itemids'][0]


def add_graph_cpu(item_id, token):
    add_graph_json = {
        "jsonrpc": "2.0",
        "method": "graph.create",
        "params": {
            "name": "CPU load",
            "width": 900,
            "height": 200,
            "gitems": [
                {
                    "itemid": item_id,
                    "color": "00AA00"
                },
            ]
        },
        "auth": token,
        "id": 1
    }
    requests.post(url=url, headers=headers, json=add_graph_json)


def create_hosts(token, host_name, template_id, group_id, ip):
    host_create_json = {
        "jsonrpc": "2.0",
        "method": "host.create",
        "params": {
            "host": host_name,
            "interfaces": [
                {
                    "type": 2,
                    "main": 1,
                    "useip": 1,
                    "ip": ip,
                    "dns": "",
                    "port": "161",
                    "details": {
                        "version": 2,
                        "community": "{$SNMP_COMMUNITY}"
                    }
                }
            ],
            "macros": [
                {
                    "macro": "{$SNMP_COMMUNITY}",
                    "value": ""  # snmp_commmunity
                }
            ],
            "templates": [
                {
                    "templateid": template_id
                }
            ],
            "groups": [
                {
                    "groupid": group_id
                }
            ]
        },
        "auth": token,
        "id": 1
    }
    create_host = requests.post(url=url, headers=headers, json=host_create_json)
    host_id = create_host.json()['result']['hostids'][0]
    interface_id = get_interface_id(host_id, token)
    item_id = get_item_id(host_id, interface_id, token)
    add_graph_cpu(item_id, token)


def main():
    group_name = ""  # group_name

    with open("files\HQ_hosts.txt", "r") as file:
        lines = file.read()
    splitted_lines = lines.split('\n')
    all_lines = len(splitted_lines)
    count = 0
    for line in lines.split('\n'):
        try:
            count += 1
            host_name = line.split(' ')[0]
            ip = line.split(' ')[1]
            token = login_in()
            group_id = get_group_id(group_name, token)
            template_id = get_template_id(token)
            create_hosts(token, host_name, template_id, group_id, ip)
            print(f"i've made zabbix host for {host_name}: {ip} ({count}/{all_lines})")
        except Exception as ex:
            print(ex)
            continue
        finally:
            logout(token)


if __name__ == '__main__':
    main()
