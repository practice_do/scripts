import logging
import netmiko
from concurrent.futures import ThreadPoolExecutor
from creds import *

logging.basicConfig(format='[%(asctime)s | [%(threadName)s] %(message)s', datefmt='%I:%M:%S')


def configure_policy_rule(ip):
    with netmiko.Netmiko(**{
        'device_type': 'huawei',
        'host': ip,
        'username': username,
        'password': pw,
        'conn_timeout': 10
    }) as ssh:
        ssh.enable()
        logging.warning(f"connected to {ip}")
        result = ssh.send_command("dis cur | i ")  # поиск политики
        splitted_result = result.split('\n')
        try:
            if ("ip address-set" in splitted_result[1]):
                commands = [""  # команды для изменения политики
                            ]
                ssh.send_config_set(commands)
                logging.warning(f'Added here -> {ip}')
        except Exception as ex:
            logging.warning(f'No object group here -> {ip}')


def main():
    with open("files\hosts_usg_result.txt", "r") as file:
        lines = file.read()
    ips = [line.split(':')[1] for line in lines.split('\n')]
    try:
        with ThreadPoolExecutor(max_workers=10) as thread:
            thread.map(configure_policy_rule, ips)
    except Exception as ex:
        logging.warning(ex)


if __name__ == '__main__':
    main()
