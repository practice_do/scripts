from concurrent.futures import ThreadPoolExecutor
from datetime import datetime
from itertools import repeat
import time
import logging

import paramiko


def show_clock(hosts, command):
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%I:%M:%S')
    output = '=======empty======='
    key_file = paramiko.RSAKey.from_private_key_file(r'C:\Users\shapochkin_v\.ssh\id_rsa')  # путь к закрытому ключу
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    host_ip = hosts.split(':')[1]
    host_name = hosts.split(':')[0]
    if host_ip != '':
        try:
            client.connect(host_ip.strip(), username='ansible', pkey=key_file)
        except Exception as ex:
            logging.warning(f"{host_name}")
            logging.warning(f"{ex}")
            return output
        channel = client.invoke_shell()
        logging.warning(f'Connected to {host_name}: {host_ip}')
        channel.send(f'{command} \n'.encode())
        time.sleep(3)
        output = channel.recv(65535).decode('utf-8')
    client.close()
    return output


def get_data(thread_number, hosts, command):
    with ThreadPoolExecutor(max_workers=thread_number) as thread:
        result = thread.map(show_clock, hosts, repeat(command))
    with open(r'files/result_multithread.txt',
              'w+') as result_f:
        for e in result:
            result_f.write(f'\n\n{e}\n\n')


def main():
    time_start = datetime.now()
    command = "dis clock"
    with open(r'files/hosts_usg_result.txt', 'r') as hosts:
        hosts_list = hosts.readlines()
    # get_data(10, hosts_list, command) # differ = 0:00:28.064344
    # get_data(20, hosts_list, command) # differ = 0:00:21.074832
    get_data(30, hosts_list, command)  # differ = 0:00:21.068942
    # get_data(40, hosts_list, command) # differ = 0:00:21.055339
    time_end = datetime.now()
    differ = time_end - time_start
    print(differ)


if __name__ == '__main__':
    main()
