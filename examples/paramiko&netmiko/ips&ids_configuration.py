import netmiko
import logging
from creds import *
import re

required_zones = []  # список зон


def configure_ips(host_ips):
    logging.basicConfig(format='%(asctime)s %(message)s', datefmt='%I:%M:%S')
    policy_rules_names = []
    for host_ip in host_ips:
        policy_rules_names.clear()
        try:
            device = {
                'device_type': 'huawei',
                'host': host_ip,
                'username': username,
                'password': pw,
                'conn_timeout': 10
            }
            with netmiko.Netmiko(**device) as ssh:
                ssh.enable()
                logging.warning(f"connected to {host_ip}")
                commands = ['profile type ips name profile_block_if_high'
                            # настройки политики
                            ]
                ssh.send_config_set(commands)
                command = 'dis security-policy rule all'
                policy_rules = ssh.send_command(command)
                pattern = '.*(permit|deny).*'
                for line in policy_rules.split('\n'):
                    if not re.match(pattern, line):
                        continue
                    splitted_line = line.split(' ')
                    splitted_line = [str for str in splitted_line if str != '']
                    if len(splitted_line) != 5:
                        diff = len(splitted_line) - 5
                        rule_name_t = splitted_line[1]
                        for i in range(2, diff + 2):
                            rule_name_t += ' ' + splitted_line[i]
                        rule_name_t = f'"{rule_name_t}"'
                        policy_rules_names.append(rule_name_t)
                        print(rule_name_t)
                    else:
                        policy_rules_names.append(splitted_line[1])
                        print(splitted_line[1])
                for rule_name in policy_rules_names:
                    if rule_name == 'default':
                        break
                    logging.warning(f'{rule_name} на хосте {host_ip}')
                    rule_zones = []
                    flag_to_send = False
                    flag_local = False
                    rule_zones.clear()
                    command = f'display security-policy rule name {rule_name}'
                    result_spec_policy_rule = ssh.send_command(command)
                    for line in result_spec_policy_rule.split('\n'):
                        if 'destination-zone' in line or 'source-zone' in line:
                            zone_name = line.lstrip().split(' ')[1]
                            if zone_name == 'local':
                                flag_local = True
                                break
                            rule_zones.append(zone_name)
                    if flag_local:
                        continue
                    for zone in required_zones:
                        if len(rule_zones) == 0 or zone in rule_zones:
                            flag_to_send = True
                            break
                    if flag_to_send:
                        commands = ['security-policy',
                                    f'rule name {rule_name}'
                                    # настройки политики
                                    ]
                        ssh.send_config_set(commands)
        except Exception as ex:
            print(ex)


def main():
    host_ips = [
        # ip-адреса устройств
    ]
    configure_ips(host_ips)


if __name__ == '__main__':
    main()
