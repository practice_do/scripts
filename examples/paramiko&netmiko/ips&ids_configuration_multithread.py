import netmiko
from concurrent.futures import ThreadPoolExecutor
import logging
from creds import *
import re

required_zones = []  # список зон
logging.basicConfig(format='[%(asctime)s | [%(threadName)s] %(message)s', datefmt='%I:%M:%S')
host_ip = ''  # ip-addr
device = {
    'device_type': 'huawei',
    'host': host_ip,
    'username': username,
    'password': pw,
    'conn_timeout': 25
}


def configure_policy_ips():
    try:
        with netmiko.Netmiko(**device) as ssh:
            ssh.enable()
            logging.warning(f"Configuring ips policy on {host_ip}")
            commands = ['profile type ips name profile_block_if_high'
                        # настройка политики
                        ]
            ssh.send_config_set(commands)
            logging.warning("IPS policy is added")
    except Exception as ex:
        logging.warning(ex)


def get_rule_names():
    try:
        policy_rules_names = []
        with netmiko.Netmiko(**device) as ssh:
            ssh.enable()
            logging.warning(f"Collecting data from {host_ip}")
            command = 'dis security-policy rule all'
            policy_rules = ssh.send_command(command)
            pattern = '.*(permit|deny).*'
            for line in policy_rules.split('\n'):
                if not re.match(pattern, line):
                    continue
                splitted_line = line.split(' ')
                splitted_line = [str for str in splitted_line if str != '']
                if len(splitted_line) != 5:
                    diff = len(splitted_line) - 5
                    rule_name_t = splitted_line[1]
                    for i in range(2, diff + 2):
                        rule_name_t += ' ' + splitted_line[i]
                    rule_name_t = f'"{rule_name_t}"'
                    policy_rules_names.append(rule_name_t)
                    # print(rule_name_t)
                else:
                    policy_rules_names.append(splitted_line[1])
                    # print(splitted_line[1])
            logging.warning("Got policies")
            return policy_rules_names
    except Exception as ex:
        logging.warning(ex)


def get_rules(rule_name):
    if rule_name == 'default':
        logging.warning("Skipping default rule...")
        return None
    try:
        with netmiko.Netmiko(**device) as ssh:
            ssh.enable()
            logging.warning(f'Collecting {rule_name}...')
            command = f'display security-policy rule name {rule_name}'
            policy_rule = ssh.send_command(command)
            # logging.warning(f'{rule_name}: {policy_rule}')
            return {rule_name: policy_rule}
    except Exception as ex:
        logging.warning(ex)
        return None


def configure_ips(rule_name):
    with netmiko.Netmiko(**device) as ssh:
        ssh.enable()
        ssh.send_config_set(['security-policy',
                             f'rule name {rule_name}'
                             # настрофки политики
                             ])
        logging.warning(f"Changed rule name {rule_name}")


def parse_rules(list_dict):
    right_rules = []
    rule_zones = []
    rule_name = ''
    for dict_rule in list_dict:
        flag_local = False
        rule_zones.clear()
        if dict_rule is not None:
            rule_tuple = list(dict_rule.items())[0]
            rule_name = rule_tuple[0]
            rule = rule_tuple[1]
            for line in rule.split('\n'):
                if 'destination-zone' in line or 'source-zone' in line:
                    zone_name = line.lstrip().split(' ')[1]
                    if zone_name == 'local':
                        flag_local = True
                        break
                    rule_zones.append(zone_name)
        if flag_local:
            continue
        for zone in required_zones:
            if len(rule_zones) == 0 or zone in rule_zones:
                right_rules.append(rule_name)
                break
    return right_rules


def main():
    configure_policy_ips()
    rule_names = get_rule_names()
    with ThreadPoolExecutor(max_workers=4) as thread:
        dicts_rules = thread.map(get_rules, rule_names)
    parsed_rules = parse_rules(dicts_rules)
    with ThreadPoolExecutor(max_workers=4) as thread:
        thread.map(configure_ips, parsed_rules)


if __name__ == '__main__':
    main()
