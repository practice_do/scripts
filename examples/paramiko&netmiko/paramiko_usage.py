import paramiko
import time


def main():
    key_file = paramiko.RSAKey.from_private_key_file(r'C:\Users\shapochkin_v\.ssh\id_rsa')  # путь к закрытому ключу
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    with open(r'files\hosts_usg_result.txt', 'r') as hosts, open(
            r'files\test_result.txt', 'w+') as result:
        lines = hosts.readlines() # строка в файле должна выглядеть так: имя ip
        for line in lines:
            host_ip = line.split(':')[1].strip()
            host_name = line.split(':')[0].strip()
            if host_ip != '':
                try:
                    client.connect(host_ip, username='ansible', pkey=key_file)
                except Exception as ex:
                    print(host_ip)
                    print(ex)
                    continue
                channel = client.invoke_shell()
                print(f'Connected to {host_name}: {host_ip}')
                channel.send('display clock \n'.encode())
                time.sleep(3)
                output = channel.recv(65535).decode('utf-8')
                result.write(output + '\n')
                result.write('#' * 30)
            else:
                continue
        client.close()


if __name__ == '__main__':
    main()
